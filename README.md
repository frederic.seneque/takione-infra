# Takione Infra
GitOps code pour déployer l'infrastructure Takione.

## Purpose
Ce contenu a été conçu à des fins de démonstration pour montrer une approche d'architecture GitOps end-to-end.

Le lien vers la conférence et le live-coding associé sera disponible prochainement.

Si vous débutez avec la stack Terraform, Ansible, Kubernetes, Helm et ArgoCD, pensez à [nous contacter](mailto:contact@takima.fr), nous accompagnons de nombreuses équipes avec de superbes formations sur le sujet.

## Variables CI

Pour fonctionner vous devrez ajouter des variables d'environnement dans votre CI :

### AWS Acces

* AWS_ACCESS_KEY_ID
* AWS_SECRET_ACCESS_KEY
* AWS_REGION

### Rancher admin password

* RANCHER_ADMIN_PWD

### Terraform Backend bucket

* TF_STATE_BUCKET
* TF_BACKEND_BUCKET_NAME


Plus globalement vous pouvez override l'ensemeble des variables terraform avec TF_VAR_nom-de-var

## License
This repository and its content are distributed under the [AGPLv3 License](LICENSE.md).

Any improvements to this codebase, or any side-code integrating part of this codebase should be made available as open-source.

Need to use it for a specific closed-source project? [Let's discuss it](mailto:contact@takima.fr)
